from glad.loader import BaseLoader

_EGL_LOADER = '''
void gladLoadEGL() {
    static void* fun(const(char)* x) {
        return eglGetProcAddress(x);
    }

    gladLoadEGL(&fun);
}
'''

_EGL_HAS_EXT = '''
private bool has_ext(const(char)* ext) {
    return true;
}
'''

class EGLDLoader(BaseLoader):
    def write(self, fobj):
        if not self.disabled:
            fobj.write(_EGL_LOADER)

    def write_begin_load(self, fobj):
        pass

    def write_find_core(self, fobj):
        pass

    def write_has_ext(self, fobj):
        fobj.write(_EGL_HAS_EXT)
