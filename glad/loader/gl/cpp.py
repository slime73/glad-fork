from glad.loader import BaseLoader
from glad.loader.cpp import LOAD_OPENGL_DLL, LOAD_OPENGL_DLL_H

_OPENGL_LOADER = \
LOAD_OPENGL_DLL % {'pre':'static', 'init':'open_gl', 'terminate':'close_gl'} + '''

#ifdef GLAD_USE_SDL
#include <SDL.h>
#if !SDL_VERSION_ATLEAST(2,0,0)
#error SDL 2 is required!
#endif
#else
#include <assert.h>
#endif

bool gladLoadGL(void) {
#ifdef GLAD_USE_SDL
    return gladLoadGLLoader(SDL_GL_GetProcAddress);
#else
    // generic gladLoadGL is not implemented, use gladLoadGLLoader or define GLAD_USE_SDL
    assert(0);
    return false;
#endif
}

struct {
    int major;
    int minor;
    int gles;
} GLVersion;

'''

_OPENGL_HAS_EXT = '''
static bool has_ext(const char *ext) {
    if(GLVersion.major < 3) {
        const char *extensions;
        const char *loc;
        const char *terminator;
        extensions = (const char *)fp_glGetString(GL_EXTENSIONS);
        if(extensions == NULL || ext == NULL) {
            return false;
        }

        while(1) {
            loc = strstr(extensions, ext);
            if(loc == NULL) {
                return false;
            }

            terminator = loc + strlen(ext);
            if((loc == extensions || *(loc - 1) == ' ') &&
                (*terminator == ' ' || *terminator == '\\0')) {
                return true;
            }
            extensions = terminator;
        }
    } else {
        int num;
        fp_glGetIntegerv(GL_NUM_EXTENSIONS, &num);

        int index;
        for(index = 0; index < num; index++) {
            const char *e = (const char*)fp_glGetStringi(GL_EXTENSIONS, index);
            if(strcmp(e, ext) == 0) {
                return true;
            }
        }
    }

    return false;
}

'''

_OPENGL_HEADER_COMMON = '''
/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2013 David Herberth, modified by Alex Szpakowski
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
 
'''

_OPENGL_HEADER = _OPENGL_HEADER_COMMON + '''
#ifndef __glad_hpp_

#if defined(__gl_h_) || defined(__glext_h_) || defined(__glcorearb_h_) \\
    || defined(__gl3_h) || defined(__gl3_ext_h)
#error OpenGL header already included, remove this include, glad already provides it
#endif

#define __glad_hpp_
#define __gl_h_

#if defined(_WIN32) && !defined(APIENTRY) && !defined(__CYGWIN__) && !defined(__SCITECH_SNAP__)
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN 1
#endif
#include <windows.h>
#endif

#ifndef APIENTRY
#define APIENTRY
#endif
#ifndef APIENTRYP
#define APIENTRYP APIENTRY *
#endif
#ifndef GLAPI
#define GLAPI extern
#endif

namespace glad {

bool gladLoadGL(void);

typedef void* (* LOADER)(const char *name);
bool gladLoadGLLoader(LOADER);
'''

_OPENGL_HEADER_LOADER = '''
''' + LOAD_OPENGL_DLL_H

_OPENGL_HEADER_END = '''
} /* namespace glad */

#endif /* __glad_hpp_ */
'''

_OPENGL_FUNCS_HEADER = _OPENGL_HEADER_COMMON + '''
#ifndef __glad_funcs_hpp_
#define __glad_funcs_hpp_

#include {0}

namespace glad {{

'''

_OPENGL_FUNCS_HEADER_END = '''
} /* namespace glad */

#endif /* __glad_funcs_hpp_ */
'''


class OpenGLCPPLoader(BaseLoader):
    def write(self, fobj):
        if not self.disabled:
            fobj.write(_OPENGL_LOADER)

    def write_begin_load(self, fobj):
        fobj.write('\tGLVersion.major = 0; GLVersion.minor = 0; GLVersion.gles = 0;\n')
        fobj.write('\tfp_glGetString = (pfn_glGetString)load("glGetString");\n')
        fobj.write('\tif(fp_glGetString == NULL) return false;\n')

    def write_find_core(self, fobj):
        fobj.write('\tconst char *v = (const char *)fp_glGetString(GL_VERSION);\n')
        fobj.write('\tint major = v[0] - \'0\', minor = v[2] - \'0\', gles = false;\n')
        fobj.write('\tGLVersion.gles = false;\n')
        fobj.write('\tif (strstr(v, "OpenGL ES ") == v) {\n')
        fobj.write('\t\tmajor = v[10] - \'0\'; minor = v[12] - \'0\'; gles = true;\n')
        fobj.write('\t}\n')
        fobj.write('\tGLVersion.major = major; GLVersion.minor = minor; GLVersion.gles = gles;\n')

    def write_has_ext(self, fobj):
        fobj.write(_OPENGL_HAS_EXT)

    def write_header(self, fobj):
        fobj.write(_OPENGL_HEADER)
        if not self.disabled:
            fobj.write(_OPENGL_HEADER_LOADER)

    def write_header_end(self, fobj):
        fobj.write(_OPENGL_HEADER_END)

    def write_funcs_header(self, fobj, includepath):
        fobj.write(_OPENGL_FUNCS_HEADER.format(includepath))

    def write_funcs_header_end(self, fobj):
        fobj.write(_OPENGL_FUNCS_HEADER_END)
