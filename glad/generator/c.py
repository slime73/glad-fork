from glad.generator import Generator
from glad.generator.util import makefiledir
from urllib import urlretrieve
import os.path
import os

KHRPLATFORM = 'https://www.khronos.org/registry/egl/api/KHR/khrplatform.h'

class CGenerator(Generator):
    def open(self):
        suffix = ''
        if not self.api == 'gl':
            suffix = '_{}'.format(self.api)

        self.h_include = '<glad/glad{}.h>'.format(suffix)
        self._f_c = open(make_path(self.path, 'src',
                                   'glad{}.c'.format(suffix)), 'w')
        self._f_h = open(make_path(self.path, 'include',
                                   'glad', 'glad{}.h'.format(suffix)), 'w')

        khr = os.path.join(self.path, 'include', 'KHR')
        khrplatform = os.path.join(khr, 'khrplatform.h')
        if not os.path.exists(khrplatform):
            if not os.path.exists(khr):
                os.makedirs(khr)
            urlretrieve(KHRPLATFORM, khrplatform)

        return self

    def close(self):
        self._f_c.close()
        self._f_h.close()

    def generate_loader(self, features, extensions):
        f = self._f_c

        if self.api == 'egl':
            features = []

        for feature in features:
            if self.api == 'gl':
                f.write('\nGLboolean GLAD_{};'.format(feature.name[3:]))
            
            f.write('\nstatic void load_{}(LOADER load) {{\n'
                    .format(feature.name))
            if self.api == 'gl':
                f.write('\tif(!GLAD_{}) return;\n'.format(feature.name[3:]))
            for func in feature.functions:
                f.write('\t{name} = (fp_{name})load("{name}");\n'
                    .format(name=func.proto.name))
            f.write('}\n')

        for ext in extensions:
            if len(list(ext.functions)) == 0:
                continue

            if self.api == 'gl':
                f.write('\nGLboolean GLAD_{};'.format(ext.name[3:]))
            
            f.write('\nstatic void load_{}(LOADER load) {{\n'
                .format(ext.name))
            if self.api == 'gl':
                f.write('\tif(!GLAD_{}) return;\n'.format(ext.name[3:]))
            if ext.name == 'GLX_SGIX_video_source': f.write('#ifdef _VL_H_\n')
            if ext.name == 'GLX_SGIX_dmbuffer': f.write('#ifdef _DM_BUFFER_H_\n')
            for func in ext.functions:
                # even if they were in written we need to load it
                f.write('\t{name} = (fp_{name})load("{name}");\n'
                    .format(name=func.proto.name))
            if ext.name in ('GLX_SGIX_video_source', 'GLX_SGIX_dmbuffer'): f.write('#endif\n')
            f.write('}\n')

        f.write('static void find_extensions(void) {\n')
        if self.api == 'gl':
            for ext in extensions:
                f.write('\tGLAD_{0} = has_ext("{0}");\n'.format(ext.name[3:]))
        f.write('}\n\n')

        f.write('static void find_core(void) {\n')
        self.loader.write_find_core(f)
        if self.api == 'gl':
            for feature in features:
                shouldgles = 0
                if feature.name.find("GL_ES_VERSION") != -1:
                    shouldgles = 1
                f.write('\tGLAD_{} = gles == {} && ((major == {num[0]} && minor >= {num[1]}) ||'
                    ' major > {num[0]});\n'.format(feature.name[3:], shouldgles, num=feature.number))
        f.write('}\n\n')

        f.write('void gladLoad{}Loader(LOADER load) {{\n'.format(self.api.upper()))

        self.loader.write_begin_load(f)
        f.write('\tfind_core();\n')

        for feature in features:
            f.write('\tload_{}(load);\n'.format(feature.name))
        f.write('\n\tfind_extensions();\n')
        for ext in extensions:
            if len(list(ext.functions)) == 0:
                continue
            f.write('\tload_{}(load);\n'.format(ext.name))
        f.write('\n\treturn;\n}\n\n')

        self.loader.write_header_end(self._f_h)

    def generate_types(self, types):
        f = self._f_h

        self.loader.write_header(f)

        for type in types:
            if self.api == 'gl' and 'khrplatform' in type.raw:
                continue

            f.write(type.raw.lstrip().replace('        ', ''))
            f.write('\n')

    def generate_features(self, features):
        f = self._f_h
        write = set()
        if self.api == 'egl':
            for feature in features:
                for func in feature.functions:
                    self.write_function_def(f, func)
        else:
            self.write_functions(f, write, set(), features)

        f = self._f_c
        f.write('#include <string.h>\n#include {}\n'.format(self.h_include))
        self.loader.write(f)
        self.loader.write_has_ext(f)

        for func in write:
            self.write_function(f, func)

    def generate_extensions(self, extensions, enums, functions):
        write = set()
        written = set(enum.name for enum in enums) | \
                    set(function.proto.name for function in functions)

        f = self._f_h
        self.write_functions(f, write, written, extensions)

        f = self._f_c
        for ext in extensions:
            if ext.name == 'GLX_SGIX_video_source': f.write('#ifdef _VL_H_\n')
            if ext.name == 'GLX_SGIX_dmbuffer': f.write('#ifdef _DM_BUFFER_H_\n')
            for func in ext.functions:
                if func in write:
                    self.write_function(f, func)
            if ext.name in ('GLX_SGIX_video_source', 'GLX_SGIX_dmbuffer'): f.write('#endif\n')

    def write_functions(self, f, write, written, extensions):
        for ext in extensions:
            f.write('\n#ifndef {0}\n#define {0} 1\n'.format(ext.name))
            if self.api == 'gl':
                f.write('extern GLboolean GLAD_{};\n'.format(ext.name[3:]))

            for enum in ext.enums:
                enumstr = "#define {}{}{}"
                tablen = 38 - len(enum.name)
                if enum.name in written:
                    enumstr = '/* duplicate: ' + enum.name + ' */'
                else:
                    enumstr = enumstr.format(enum.name, max(tablen, 1) * ' ', enum.value)
                f.write(enumstr + '\n')
                written.add(enum.name)

            if ext.name == 'GLX_SGIX_video_source': f.write('#ifdef _VL_H_\n')
            if ext.name == 'GLX_SGIX_dmbuffer': f.write('#ifdef _DM_BUFFER_H_\n')

            for func in ext.functions:
                is_duplicate = func.proto.name in written
                if is_duplicate:
                    f.write('/* duplicate: {} {}({}); */'.format(func.proto.ret.to_c(),
                                                      func.proto.name,
                        ', '.join(param.type.to_c() for param in func.params)))
                else:
                    f.write('typedef {} (APIENTRYP fp_{})({});'.format(func.proto.ret.to_c(),
                                                      func.proto.name,
                        ', '.join(param.type.to_c() + ' ' + param.name for param in func.params)))

                f.write('\n')
                
                # if not is_duplicate:
                    # f.write('GLAPI fp_{0} glad{0};\n'.format(func.proto.name))
                    # f.write('#define {0} glad{0}\n'.format(func.proto.name))
                
            for func in ext.functions:
                is_duplicate = func.proto.name in written
                if not is_duplicate:
                    f.write('GLAPI fp_{0} glad{0};\n'.format(func.proto.name))
                    f.write('#define {0} glad{0}\n'.format(func.proto.name))

                write.add(func)
                written.add(func.proto.name)

            if ext.name in ('GLX_SGIX_video_source', 'GLX_SGIX_dmbuffer'): f.write('#endif\n')

            f.write('#endif /* {0} */\n'.format(ext.name))

    def write_extern(self, fobj):
        fobj.write('#ifdef __cplusplus\nextern "C" {\n#endif\n')

    def write_extern_end(self, fobj):
        fobj.write('#ifdef __cplusplus\n}\n#endif\n')

    def write_function_def(self, fobj, func):
        fobj.write('{} {}('.format(func.proto.ret.to_c(), func.proto.name))
        fobj.write(', '.join(param.type.to_c() + ' ' + param.name for param in func.params))
        fobj.write(');\n')

    def write_function_prototype(self, fobj, func, is_duplicate):
        fobj.write('typedef {} (APIENTRYP fp_{})({});\n'.format(func.proto.ret.to_c(),
                                                      func.proto.name,
                        ', '.join(param.type.to_c() + ' ' + param.name for param in func.params)))
        fobj.write('GLAPI fp_{0} glad{0};\n'.format(func.proto.name))
        fobj.write('#define {0} glad{0}\n'.format(func.proto.name))

    def write_function(self, fobj, func):
        fobj.write('fp_{0} glad{0};\n'.format(func.proto.name))


def make_path(path, *args):
    path = os.path.join(path, *args)
    makefiledir(path)
    return path
