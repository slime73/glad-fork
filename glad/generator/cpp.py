from glad.generator import Generator
from glad.generator.util import makefiledir
from urllib import urlretrieve
import os.path
import os

KHRPLATFORM = 'https://www.khronos.org/registry/egl/api/KHR/khrplatform.h'

class CPPGenerator(Generator):
    def open(self):
        suffix = ''
        if not self.api == 'gl':
            suffix = '_{}'.format(self.api)

        self.h_include = '"glad{}.hpp"'.format(suffix)
        self.func_h_include = '"glad{}funcs.hpp"'.format(suffix)
        self._f_c = open(make_path(self.path, 'src',
                                   'glad{}.cpp'.format(suffix)), 'w')
        self._f_h = open(make_path(self.path, 'include',
                                   'glad', 'glad{}.hpp'.format(suffix)), 'w')
        self._f_func_h = open(make_path(self.path, 'include',
                                   'glad', 'glad{}funcs.hpp'.format(suffix)), 'w')

        khr = os.path.join(self.path, 'include', 'KHR')
        khrplatform = os.path.join(khr, 'khrplatform.h')
        if not os.path.exists(khrplatform):
            if not os.path.exists(khr):
                os.makedirs(khr)
            urlretrieve(KHRPLATFORM, khrplatform)

        return self

    def close(self):
        self._f_c.close()
        self._f_h.close()

    def generate_loader(self, features, extensions):
        f = self._f_c

        if self.api == 'egl':
            features = []

        for feature in features:
            if self.api == 'gl':
                f.write('\nGLboolean GLAD_{} = GL_FALSE;\n'.format(feature.name[3:]))
            
            f.write('static void load_{}(LOADER load) {{\n'
                    .format(feature.name))
            if self.api == 'gl':
                f.write('\tif(!GLAD_{}) return;\n'.format(feature.name[3:]))
            for func in feature.functions:
                f.write('\tfp_{name} = (pfn_{name})load("{name}");\n'
                    .format(name=func.proto.name))
            f.write('}\n')

        for ext in extensions:
            if self.api == 'gl':
                f.write('\nGLboolean GLAD_{} = GL_FALSE;'.format(ext.name[3:]))
            
            if len(list(ext.functions)) == 0:
                continue

            f.write('\nstatic void load_{}(LOADER load) {{\n'
                .format(ext.name))
            if self.api == 'gl':
                f.write('\tif(!GLAD_{}) return;\n'.format(ext.name[3:]))
            if ext.name == 'GLX_SGIX_video_source': f.write('#ifdef _VL_H_\n')
            if ext.name == 'GLX_SGIX_dmbuffer': f.write('#ifdef _DM_BUFFER_H_\n')
            for func in ext.functions:
                # even if they were in written we need to load it
                f.write('\tfp_{name} = (pfn_{name})load("{name}");\n'
                    .format(name=func.proto.name))
            if ext.name in ('GLX_SGIX_video_source', 'GLX_SGIX_dmbuffer'): f.write('#endif\n')
            f.write('}\n')

        f.write('\nstatic void find_extensions(void) {\n')
        if self.api == 'gl':
            for ext in extensions:
                f.write('\tGLAD_{0} = has_ext("{1}");\n'.format(ext.name[3:], ext.name))
        f.write('}\n\n')

        f.write('static void find_core(void) {\n')
        self.loader.write_find_core(f)
        if self.api == 'gl':
            for feature in features:
                shouldgles = 0
                if feature.name.find("GL_ES_VERSION") != -1:
                    shouldgles = 1
                f.write('\tGLAD_{} = gles == {} && ((major == {num[0]} && minor >= {num[1]}) ||'
                    ' major > {num[0]});\n'.format(feature.name[3:], shouldgles, num=feature.number))
        f.write('}\n\n')

        f.write('bool gladLoad{}Loader(LOADER load) {{\n'.format(self.api.upper()))

        self.loader.write_begin_load(f)
        f.write('\tfind_core();\n')

        for feature in features:
            f.write('\tload_{}(load);\n'.format(feature.name))
        f.write('\n\tfind_extensions();\n')
        for ext in extensions:
            if len(list(ext.functions)) == 0:
                continue
            f.write('\tload_{}(load);\n'.format(ext.name))
        f.write('\treturn true;\n')
        f.write('}\n')
        f.write('\n} /* namespace glad */\n')

        self.loader.write_header_end(self._f_h)
        self.loader.write_funcs_header_end(self._f_func_h)

    def generate_types(self, types):
        f = self._f_h

        self.loader.write_header(f)
        self.loader.write_funcs_header(self._f_func_h, self.h_include)
        
        for type in types:
            if self.api == 'gl' and 'khrplatform' in type.raw:
                continue
                
            if not self.spec.NAME in ('egl',) and 'khronos' in type.raw:
                continue

            f.write(type.raw.lstrip().replace('        ', ''))
            f.write('\n')

    def generate_features(self, features):
        f = self._f_h
        write = set()
        
        written = set()
        
        if self.api == 'egl':
            for feature in features:
                for func in feature.functions:
                    self.write_function_def(f, func)
        else:
            self.write_functions(f, write, written, features)

        f = self._f_c
        f.write('#include <string.h>\n#include {}\n'.format(self.h_include))
        self.loader.write(f)
        self.loader.write_has_ext(f)

        written2 = set()
        for func in write:
            if not func.proto.name in written2:
                self.write_function(f, func)
                written2.add(func.proto.name)

    def generate_extensions(self, extensions, enums, functions):
        write = set()
        written = set(enum.name for enum in enums) | \
                    set(function.proto.name for function in functions)

        f = self._f_h
        self.write_functions(f, write, written, extensions)
        
        written2 = set(enum.name for enum in enums) | \
                    set(function.proto.name for function in functions)

        f = self._f_c
        for ext in extensions:
            if ext.name == 'GLX_SGIX_video_source': f.write('#ifdef _VL_H_\n')
            if ext.name == 'GLX_SGIX_dmbuffer': f.write('#ifdef _DM_BUFFER_H_\n')
            for func in ext.functions:
                if func in write:
                    if not func.proto.name in written2:
                        self.write_function(f, func)
                        written2.add(func.proto.name)
            if ext.name in ('GLX_SGIX_video_source', 'GLX_SGIX_dmbuffer'): f.write('#endif\n')

    def write_functions(self, f, write, written, extensions):
        for ext in extensions:
            f.write('\n /* {} */\n'.format(ext.name))
            # f.write('\n#ifndef {0}\n#define {0} 1\n'.format(ext.name))
            if self.api == 'gl':
                f.write('extern GLboolean GLAD_{};\n'.format(ext.name[3:]))

            if ext.name == 'GLX_SGIX_video_source': f.write('#ifdef _VL_H_\n')
            if ext.name == 'GLX_SGIX_dmbuffer': f.write('#ifdef _DM_BUFFER_H_\n')
            
            for enum in ext.enums:
                enumstr = "#define {}{} {}\n"
                tablen = 38 - len(enum.name)
                if not enum.name in written:
                    enumstr = enumstr.format(enum.name, max(tablen, 1) * ' ', enum.value)
                    f.write(enumstr)
                    written.add(enum.name)
                    
            firstfunc = True

            for func in ext.functions:
                is_duplicate = func.proto.name in written
                if is_duplicate:
                    continue

                f.write('typedef {} (APIENTRYP pfn_{}) ({});\n'.format(func.proto.ret.to_c(),
                                                     func.proto.name,
                       ', '.join(param.type.to_c() for param in func.params)))
                
                f.write('extern pfn_{0} fp_{0};\n'.format(func.proto.name))

                if firstfunc:
                    self._f_func_h.write('\n/* {0} */\n'.format(ext.name))
                    
                self._f_func_h.write('inline {0} {1}({2}) {{ {4}fp_{1}({3}); }}\n'.format(func.proto.ret.to_c(),
                                                     func.proto.name,
                       ', '.join(param.type.to_c() + ' ' + param.name for param in func.params),
                       ', '.join(param.name for param in func.params),
                       "" if func.proto.ret.to_c() == "void" else "return "))
                    
                firstfunc = False

            for func in ext.functions:
                write.add(func)
                written.add(func.proto.name)

            if ext.name in ('GLX_SGIX_video_source', 'GLX_SGIX_dmbuffer'): f.write('#endif\n')

    def write_extern(self, fobj):
        fobj.write('#ifdef __cplusplus\nextern "C" {\n#endif\n')

    def write_extern_end(self, fobj):
        fobj.write('#ifdef __cplusplus\n}\n#endif\n')

    def write_function_def(self, fobj, func):
        fobj.write('{} {}('.format(func.proto.ret.to_c(), func.proto.name))
        fobj.write(', '.join(param.type.to_c() + ' ' + param.name for param in func.params))
        fobj.write(');\n')

    def write_function_prototype(self, fobj, func, is_duplicate):
        fobj.write('typedef {} (APIENTRYP pfn_{})({});\n'.format(func.proto.ret.to_c(),
                                                      func.proto.name,
                        ', '.join(param.type.to_c() + ' ' + param.name for param in func.params)))
        fobj.write('GLAPI pfn_{0} glad{0};\n'.format(func.proto.name))
        fobj.write('#define {0} glad{0}\n'.format(func.proto.name))

    def write_function(self, fobj, func):
        # fobj.write('typedef {} (APIENTRYP pfn_{})({});\n'.format(func.proto.ret.to_c(),
                                                     # func.proto.name,
                       # ', '.join(param.type.to_c() for param in func.params)))
        fobj.write('pfn_{0} fp_{0};\n'.format(func.proto.name))


def make_path(path, *args):
    path = os.path.join(path, *args)
    makefiledir(path)
    return path
